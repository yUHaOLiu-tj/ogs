Implemented energy split models are:
- isotropic model \cite bourdin2000numerical, (energy_split_model="Isotropic")
- volumetric-deviatoric split model \cite amor2009regularized (energy_split_model="VolumetricDeviatoric")
